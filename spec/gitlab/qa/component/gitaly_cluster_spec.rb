# frozen_string_literal: true

module Gitlab
  module QA
    describe Component::GitalyCluster do
      describe '#run_gitaly_cluster' do
        subject(:gitaly_cluster) { described_class.new }

        let(:postgres) { spy('postgres') }
        let(:praefect) { spy('praefect') }
        let(:gitaly) { spy('gitaly') }

        before do
          stub_const('Gitlab::QA::Component::PostgreSQL', postgres)
          stub_const('Gitlab::QA::Component::Praefect', praefect)
          stub_const('Gitlab::QA::Component::Gitaly', gitaly)
        end

        it 'starts a praefect database' do
          gitaly_cluster.run_gitaly_cluster(Gitlab::QA::Release.new('EE')).join
          expect(postgres).to have_received(:new).exactly(1).times
        end

        it 'starts a praefect node' do
          gitaly_cluster.run_gitaly_cluster(Gitlab::QA::Release.new('EE')).join
          expect(praefect).to have_received(:new).exactly(1).times
        end

        it 'starts 3 gitaly nodes' do
          gitaly_cluster.run_gitaly_cluster(Gitlab::QA::Release.new('EE')).join
          expect(gitaly).to have_received(:new).exactly(3).times
        end
      end
    end
  end
end
